<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Team;
use App\Models\Tournament;
use Illuminate\Http\Request;

/**
 * Class TournamentsController
 * @package App\Http\Controllers
 */
class TournamentsController extends Controller
{
    /**
     * Current division count
     */
    const DIVISION_COUNT = 2;

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = null)
    {
        if ($id) {
            $tournament = Tournament::findOrFail($id);
        } else {
            $tournament = Tournament::latest()->first();
        }

        if (!$tournament) {
            $tournament = new Tournament;
        }

        $best_teams = $this->getBestTeams($tournament->id);
        $results = $this->getResults($tournament->id);

        return view('tables', [
            'tournament' => $tournament,
            'division_count' => self::DIVISION_COUNT,
            'best_teams' => $best_teams,
            'results' => $results
        ]);
    }

    /**
     * @param int $count
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateTeams($count = 10)
    {
        $tournament = Tournament::create([
            'name' => 'Some Tournament'
        ]);

        for ($division = 0; $division < self::DIVISION_COUNT; $division++) {
            for ($i = 0; $i < $count; $i++) {
                Team::create([
                    'name' => 'Team ' . ($i + 1),
                    'division' => ($division + 1),
                    'tournament_id' => $tournament->id
                ]);
            }
        }

        return redirect(action('TournamentsController@index', ['id' => $tournament->id]));
    }

    /**
     * @param $tournament_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateScore($tournament_id)
    {
        $tournament = Tournament::findOrFail($tournament_id);

        $ids = $this->getTournamentTeams($tournament_id);
        Game::where(function ($query) use ($ids) {
            $query->whereIn('team_1_id', $ids)
                ->orWhereIn('team_2_id', $ids);
        })->delete();

        for ($division = 1; $division <= self::DIVISION_COUNT; $division++) {
            foreach ($tournament->teams->where('division', $division) as $team1) {
                foreach ($tournament->teams->where('division', $division) as $team2) {
                    if ($team1 == $team2) continue;
                    if (Game::where('team_1_id', $team2->id)->where('team_2_id', $team1->id)->first()) continue;

                    //some random logic
                    $result1 = rand(0, 5);
                    $result2 = rand(0, $result1);
                    if (rand(0, 1)) { //gamble which team will win the game
                        $result2 = rand($result1 + 1, $result1 + 5);
                    }

                    $data = [
                        'team_1_id' => $team1->id,
                        'team_2_id' => $team2->id,
                        'team_1_result' => $result1,
                        'team_2_result' => $result2,
                        'final_game' => 0
                    ];
                    Game::create($data);

                    //store same result for second team as main
                    $data['team_1_id'] = $team2->id;
                    $data['team_2_id'] = $team1->id;
                    Game::create($data);
                }
            }
        }

        return redirect(action('TournamentsController@index', ['id' => $tournament->id]));
    }

    /**
     * @param $tournament_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateResults($tournament_id)
    {
        $ids = $this->getTournamentTeams($tournament_id);

        Game::where('final_game', '>', 0)->where(function ($query) use ($ids) {
            $query->whereIn('team_1_id', $ids)
                ->orWhereIn('team_2_id', $ids);
        })->delete();

        $best_teams = $this->getBestTeams($tournament_id);
        $teams_won = [];

        for ($division = 1; $division <= self::DIVISION_COUNT; $division++) {
            for ($i = 0; $i < (count($best_teams[$division]) / 2); $i++) {
                $result1 = rand(1, 5);
                $result2 = rand(2, $result1 - 1);
                if (rand(0, 1)) { //gamble which team will win the game
                    $result2 = rand($result1 + 1, $result1 + 5);
                }

                Game::create($data = [
                    'team_1_id' => $best_teams[$division][$i]->id,
                    'team_2_id' => $best_teams[$division][count($best_teams[$division]) - 1 - $i]->id,
                    'team_1_result' => $result1,
                    'team_2_result' => $result2,
                    'final_game' => 1
                ]);

                $teams_won[] = $result1 > $result2 ? $best_teams[$division][$i] : $best_teams[$division][count($best_teams[$division]) - 1 - $i];
            }
        }

        $teams_won2 = [];
        for ($i = 0; $i < (count($teams_won) / 2); $i++) {
            $result1 = rand(1, 5);
            $result2 = rand(2, $result1 - 1);
            if (rand(0, 1)) { //gamble which team will win the game
                $result2 = rand($result1 + 1, $result1 + 5);
            }

            Game::create($data = [
                'team_1_id' => $teams_won[$i]->id,
                'team_2_id' => $teams_won[count($teams_won) - 1 - $i]->id,
                'team_1_result' => $result1,
                'team_2_result' => $result2,
                'final_game' => 2
            ]);

            $teams_won2[] = $result1 > $result2 ? $teams_won[$i] : $teams_won[count($teams_won) - 1 - $i];
        }

        for ($i = 0; $i < (count($teams_won2) / 2); $i++) {
            $result1 = rand(1, 5);
            $result2 = rand(2, $result1 - 1);
            if (rand(0, 1)) { //gamble which team will win the game
                $result2 = rand($result1 + 1, $result1 + 5);
            }

            Game::create($data = [
                'team_1_id' => $teams_won2[$i]->id,
                'team_2_id' => $teams_won2[count($teams_won2) - 1 - $i]->id,
                'team_1_result' => $result1,
                'team_2_result' => $result2,
                'final_game' => 3
            ]);
        }

        return redirect(action('TournamentsController@index', ['id' => $tournament_id]));
    }

    /**
     * @param $tournament_id
     * @return array
     */
    private function getBestTeams($tournament_id)
    {
        if (!$tournament_id) return [];

        $ids = $this->getTournamentTeams($tournament_id);
        $games = Game::where(function ($query) use ($ids) {
            $query->whereIn('team_1_id', $ids)
                ->orWhereIn('team_2_id', $ids);
        })->get();
        if (!$games->count()) return [];

        $tournament = Tournament::findOrFail($tournament_id);

        $best_teams = [];
        for ($division = 1; $division <= self::DIVISION_COUNT; $division++) {
            $teams = [];
            foreach ($tournament->teams->where('division', $division) as $team) {
                foreach ($team->games as $game) {
                    if ($game->team_1_result > $game->team_2_result) {
                        $team->score += 1;
                    }
                }
                $teams[] = $team;
            }

            usort($teams, function ($a, $b) {
                return $a->score < $b->score;
            });
            $best_teams[$division] = array_slice($teams, 0, 4);
        }

        return $best_teams;
    }

    /**
     * @param $tournament_id
     * @return array
     */
    private function getResults($tournament_id)
    {
        if (!$tournament_id) return [];
        $ids = $this->getTournamentTeams($tournament_id);

        $results = Game::where('final_game', '>', 0)->where(function ($query) use ($ids) {
            $query->whereIn('team_1_id', $ids)
                ->orWhereIn('team_2_id', $ids);
        })->get();

        $results_grouped = [];
        foreach ($results as $result) {
            $results_grouped[$result->final_game][$result->team_1_id] = $result;
        }

        return $results_grouped;
    }

    private function getTournamentTeams($tournament_id) {
        if (!$tournament_id) return [];

        $tournament = Tournament::findOrFail($tournament_id);

        $ids = [];
        foreach ($tournament->teams as $team) {
            $ids[] = $team->id;
        }
        return $ids;
    }
}
