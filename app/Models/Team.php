<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = ['name', 'tournament_id', 'division'];

    public function games() {
        return $this->hasMany(Game::class, 'team_1_id', 'id')->where('final_game', 0)->orderBy('team_2_id', 'ASC');
    }
}
