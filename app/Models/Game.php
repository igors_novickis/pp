<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';
    protected $fillable = ['team_1_id', 'team_2_id', 'team_1_result', 'team_2_result', 'final_game'];

    public function team_1() {
        return $this->belongsTo(Team::class, 'team_1_id', 'id');
    }

    public function team_2() {
        return $this->belongsTo(Team::class, 'team_2_id', 'id');
    }
}
