<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <title>Tournaments</title>
</head>
<body>

<section class="main-section">
    @for ($division = 1; $division <= $division_count; $division++)
        <table class="game-table">
            <thead>
            <tr>
                <td colspan="{{ $tournament->teams->where('division', $division)->count() + 2 }}">
                    Division {{ $division }}</td>
            </tr>
            <tr>
                <td>Teams</td>
                @foreach ($tournament->teams->where('division', $division) as $team)
                    <td>{{ $team->name }}</td>
                @endforeach
                <td>Score</td>
            </tr>
            </thead>
            <tbody>
            @foreach ($tournament->teams->where('division', $division) as $team)
                <tr>
                    <td>{{ $team->name }}</td>

                    @for ($i = 0; $i < ($tournament->teams->where('division', $division)->count()); $i++)
                        @php
                            $j = $i;
                            if (($loop->iteration - 1) < $i) {
                                $j--;
                            }

                            if (isset($team->games[$j])) {
                                if ($team->games[$j]->team_1_result > $team->games[$j]->team_2_result) {
                                    $team->score += 1;
                                }
                            }
                        @endphp
                        @if (($loop->iteration - 1) == $i)
                            <td class="disabled"></td>
                        @else
                            <td>{{ isset($team->games[$j]) ? $team->games[$j]->team_1_result : '' }}
                                :
                                {{ isset($team->games[$j]) ? $team->games[$j]->team_2_result : ''}}</td>
                        @endif
                    @endfor

                    <td>{{ isset($team->score) ? $team->score : '0' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endfor

    <section class="results-section">
        <table class="results-table">
            <thead>
            <tr>
                <td></td>
                <td>Semi - Final</td>
                <td>Final</td>
            </tr>
            </thead>
            <tbody>

            @if ($results)
                <tr>
                    @for ($i = 1; $i <= 3; $i++)
                        <td>
                            @foreach($results[$i] as $result)
                                <div class="final-teams">
                                    {{ $result->team_1->name }} (Division: {{ $result->team_1->division }})
                                    <hr/>
                                    {{ $result->team_2->name }}
                                    (Division: {{ $result->team_2->division }})
                                </div>
                                <div class="final-results">
                                    {{ $result->team_1_result }}
                                    :
                                    {{ $result->team_2_result }}
                                </div>
                            @endforeach
                        </td>
                    @endfor
                </tr>
            @endif
            </tbody>
        </table>
    </section>
</section>

<section class="generate-section">
    <h2>Generate</h2>
    <div>
        <button onclick="window.location.href = '{{ action('TournamentsController@generateTeams') }}'">Generate Teams
        </button>
    </div>

    @if ($tournament->id)
        <div>
            <button onclick="window.location.href = '{{ action('TournamentsController@generateScore', ['id' => $tournament->id]) }}'">
                Generate Scores
            </button>
        </div>
    @endif

    @if ($best_teams)
        <div>
            <button onclick="window.location.href = '{{ action('TournamentsController@generateResults', ['id' => $tournament->id]) }}'">
                Generate Final Results
            </button>
        </div>
    @endif
</section>

</body>
</html>
