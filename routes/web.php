<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{id?}', 'TournamentsController@index');
Route::get('generate/teams', 'TournamentsController@generateTeams');
Route::get('generate/score/{id?}', 'TournamentsController@generateScore');
Route::get('generate/results/{id?}', 'TournamentsController@generateResults');