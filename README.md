# README #

Some basic tournaments app with data generators;

### Setup Requirements ###

* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* BCMath PHP Extension

### Installation ###

* Rename .env.example file to .env
* Configure database credentials inside .env file
* Run `php artisan key:generate` inside project directory
* Run `php artisan migrate` inside project directory
* Use `php artisan serve` to serve project or run it on your own custom web server
